package ictgradschool.industry.lab10.QuizOne;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ShuffleStrings {
    public static void main(String[] args) {
        Scanner s = new Scanner("path");
        System.out.println(shuffleStrings(s.nextLine().split("\\s+")));

    }

    public static List<String> shuffleStrings(String[] strings){
        List<String> stringList = Arrays.asList(strings);
        Collections.shuffle(stringList);
        return stringList;
    }
}
