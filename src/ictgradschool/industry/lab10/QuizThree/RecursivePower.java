package ictgradschool.industry.lab10.QuizThree;

public class RecursivePower {
    public static void main(String[] args) {


    }

    private static int power(int x, int y){
        if (y > 0){
            return power(x, y-1) * x;
        }
        else if (y < 0){
            return power(x, y+1) / x;
        }
        else
            return 1;
    }

    public static String reverseString(String s){
        if (s.length() <= 1)
            return s;
        return reverseString(s.substring(1)) + s.charAt(0);
    }
}
