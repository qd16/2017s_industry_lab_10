package ictgradschool.industry.lab10.QuizTwo;

import java.util.HashMap;
import java.util.Scanner;

public class PeoplesWeight {

    private static HashMap<String, Double> peoplesWeight;

    public static void main(String[] args) {
        readData();
        printData();
    }

    public static void readData(){
        Scanner s = new Scanner("path");
        s.useDelimiter(",");
        while(s.hasNextLine()){
            String name = s.next();
            Double weight = Double.parseDouble(s.next());
            peoplesWeight.put(name, weight);
        }
    }

    public static void printData(){
        peoplesWeight.forEach((k,v) -> System.out.println(k+" "+v));
    }
}
